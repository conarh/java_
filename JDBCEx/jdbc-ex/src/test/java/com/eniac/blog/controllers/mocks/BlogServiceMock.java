package com.eniac.blog.controllers.mocks;

import com.eniac.blog.mvc.BlogService;

public class BlogServiceMock extends BlogService{
	private String lastPost = null;

	@Override
	public void addPost(String post) {
		lastPost = post;
	}

	@Override
	public void addComment(int id, String comment) {
		
	}

	public String getLastPost(){
		return lastPost;
	}
}
