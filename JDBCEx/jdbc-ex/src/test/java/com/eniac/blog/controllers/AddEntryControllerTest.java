package com.eniac.blog.controllers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.eniac.blog.controllers.mocks.BlogServiceMock;
import static org.junit.Assert.*;

public class AddEntryControllerTest {
	private AddEntryController controller = new AddEntryController();
	private BlogServiceMock blogServiceMock = new BlogServiceMock();
	
	@Before
	public void init(){
		ReflectionTestUtils.setField(controller, "blogService", blogServiceMock);
	}
	
	@After
	public void tearDown(){
		
	}
	
	@Test
	public void testAddPostEmpty(){
		assertEntryCreation("", null);
	}
	
	@Test
	public void testAddPostSpaces(){
		assertEntryCreation("     ", null);
	}
	
	@Test
	public void testAddPostTextAndSpaces(){
		assertEntryCreation("  sometext with spaces within   ", "sometext with spaces within");
	}
	
	@Test
	public void testAddPostText(){
		assertEntryCreation("sometext with spaces within", "sometext with spaces within");
	}
	
	private void assertEntryCreation(final String entry, final String expectedResult){
		controller.addPost(entry);
		if (expectedResult != null){
			assertEquals("Invalid argument sent to blogService", expectedResult, blogServiceMock.getLastPost());
		} else {
			assertNull("Method blogService.addPost was called", blogServiceMock.getLastPost());
		}
	}

}
