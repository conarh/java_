package com.eniac.blog.mvc;

import org.apache.commons.lang3.StringUtils;

public class AddCommentFormBean {
	private String comment;
	private int id;
	
	public String getComment() {
		return comment.trim();
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean hasComment(){
		return StringUtils.trimToNull(comment)!=null;
	}
	
}
