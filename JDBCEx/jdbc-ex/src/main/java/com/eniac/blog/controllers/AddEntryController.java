package com.eniac.blog.controllers;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.eniac.blog.mvc.BlogService;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class AddEntryController {
	private static Logger LOG = LoggerFactory.getLogger(AddEntryController.class);
	
	@Autowired
	private BlogService blogService;

	@RequestMapping(value = "addentry.html", method=POST)
	public String addPost(@RequestParam("entry") String entry){
		entry = StringUtils.trimToNull(entry);
		if (entry!=null){
			blogService.addPost(entry);
    	} else {
    		LOG.warn("Post is empty!");
    	}
		return "redirect:index.html";
	}
	
}
