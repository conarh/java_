package com.eniac.blog.dao.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DOmain object for blog
 * 
 * @author Conarh
 */
@Entity
@Table(name="posts")
public class BlogPost {
	@Id
	@Column(name="idposts")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="post", columnDefinition="TEXT")
	private String entry;
	
	@OneToMany(mappedBy="blogPost", fetch=FetchType.EAGER)
	private List<BlogPostComment> comments = new ArrayList<>();
	
	public String getEntry() {
		return entry;
	}
	
	public void setEntry(String entry) {
		this.entry = entry;
	}

	public List<BlogPostComment> getComments() {
		return comments;
	}
	
	public BlogPostComment addComment(String comment){
		final BlogPostComment blogPostComment = new BlogPostComment();
		blogPostComment.setComment(comment);
		blogPostComment.setBlogPost(this);
		comments.add(blogPostComment);
		return blogPostComment;
	}

	public int getId() {
		return id;
	}
}
