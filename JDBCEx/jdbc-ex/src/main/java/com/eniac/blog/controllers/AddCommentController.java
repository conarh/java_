package com.eniac.blog.controllers;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.eniac.blog.mvc.AddCommentFormBean;
import com.eniac.blog.mvc.BlogService;

@Controller
public class AddCommentController {
	private static Logger LOG = LoggerFactory.getLogger(AddCommentController.class);
	
	@Autowired
	private BlogService blogService;

	@RequestMapping(value="addcomment.html", method=POST)
	public String addComment(AddCommentFormBean comment){
		if (comment.hasComment()){
    		blogService.addComment(comment.getId(), comment.getComment());
    	} else {
    		LOG.warn("Comment is empty!");
    	}
		return "redirect:/index.html";
	}
}
