package com.eniac.blog.dao.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
public class BlogPostComment {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idcomments")
	private int id;

	private String comment;
	
	@ManyToOne
	@JoinColumn(name="post", nullable=false)
	private BlogPost blogPost;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public BlogPost getBlogPost() {
		return blogPost;
	}

	public void setBlogPost(BlogPost post) {
		this.blogPost = post;
	}
	
}
