package com.eniac.blog.mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.eniac.blog.dao.BlogDAO;


public class BlogService {
	private static final Logger LOG = LoggerFactory.getLogger(BlogService.class);
	@Autowired
	private BlogDAO dao;

	public void addPost(String post){
		dao.addPost(post);
		LOG.debug("Post added: {}...", post.substring(0, Math.min(5, post.length())));
	}
	
	public void addComment(int id, String comment){
		dao.addComment(id, comment);
		LOG.debug("Comment added for post {}: {}...", id, comment.substring(0, Math.min(5, comment.length())));
	}
		
}
