package com.eniac.blog.mvc;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.eniac.blog.dao.BlogDAO;
import com.eniac.blog.dao.domain.BlogPost;



public class BlogView {
	private static Logger LOG = LoggerFactory.getLogger(BlogView.class);
	
	@Autowired
	private BlogDAO dao;
	
	public BlogView(){
		LOG.info("BlogView created");
	}
	
	public List<BlogPost> getAllPosts(){
		return dao.getAllPosts();
	}

	
}
