package com.eniac.blog.controllers;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.eniac.blog.mvc.BlogView;

@Controller
@RequestMapping("index.html")
public class IndexController {
	
	@Autowired
	private BlogView blogView;
	
	@RequestMapping(method=GET)
	public String get(final ModelMap model){
		model.addAttribute("blogView", blogView);
		return "index";
	}
}
