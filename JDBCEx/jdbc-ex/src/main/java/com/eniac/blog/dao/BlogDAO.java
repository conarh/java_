package com.eniac.blog.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.eniac.blog.dao.domain.BlogPost;
import com.eniac.blog.dao.domain.BlogPostComment;

/**
 * Simple JDBC DAO
 * 
 * @author Conarh
 */
// @Component
@Repository("dao")
public class BlogDAO {
	@Autowired
	private SessionFactory factory;

	public void addPost(String post) {
		final Session session = factory.openSession();
		try{
			BlogPost blogPost = new BlogPost();
			blogPost.setEntry(post);
			session.save(blogPost);
			System.out.println();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<BlogPost> getAllPosts() {
		final Session session = factory.openSession();
		try{
			final List<BlogPost> posts = session.createQuery("from BlogPost").list();
			return posts;
		} finally {
			session.close();
		}
	}

	public void addComment(final int postId, final String comment) {
		final Session session = factory.openSession();
		try{
			final BlogPost post = (BlogPost) session.load(BlogPost.class, postId);
			final BlogPostComment postComment = post.addComment(comment);
			session.save(postComment);
		} finally {
			session.close();
		}
	}

}
