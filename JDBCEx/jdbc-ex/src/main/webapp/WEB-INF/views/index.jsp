<%@page pageEncoding="utf-8" %>
<%@page import="org.springframework.web.context.support.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
	<title>Simple blog</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<c:forEach var="post" items="${blogView.allPosts}">
	<c:out value="${post.entry}"/>
	<ul>
	<c:forEach var="comment" items="${post.comments}">
		<li><c:out value="${comment.comment}"/>
	</c:forEach>
	</ul>
	<form method="post" action="addcomment.html">
		<input type="text" name="comment" size="50">
		<input type="hidden" name="id" value="<c:out value="${post.id}"/>">
		<input type="submit" value="Comment">
	</form>
	<hr>
</c:forEach>
<form method="post" action="addentry.html">
	<h3>Enter post here:</h3>
	<textarea name="entry" rows="10" cols="50"></textarea>
	<br>
	<input type="submit" value="Save">
</form>
</body>
</html>
