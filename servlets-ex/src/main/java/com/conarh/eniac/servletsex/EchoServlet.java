/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conarh.eniac.servletsex;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Conarh
 */
public class EchoServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("cp1251");
        final String message = req.getParameter("message");
        resp.setContentType("text/html; charset=cp1251");
        final PrintWriter w = resp.getWriter();
        w.println("<html><head>\n"
            + "<title>Echo form</title>\n"
            + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1251\">\n"
//            + "<meta charset=\"windows-1251\">\n"
            + "</head><body>\n"
            + "<h3>Message:</h3>\n"
            + "<pre>\n"
            + (message != null ? message : "")
            + "</pre>\n"
            + "<hr>\n"
            + "<form action=\"echo.html\" method=\"post\" >\n"
            + "Enter message:<br>\n"
            + "<textarea name=\"message\" cols=40 rows=20>\n"
            + "</textarea><br>\n"
            + "<input type=\"submit\" value=\"Отправить\">\n"
            + "</form>\n"
            + "</body></html>");
        
    }

    
}
