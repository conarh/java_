/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conarh.eniac.servletsex;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Conarh
 */
public class ContextInfoServlet extends HttpServlet {
    private int count = 0;
    
    @Override
    public void init(){
        System.out.println(getServletConfig().getServletName()+" started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain; charset=utf-8");
        final PrintWriter w = resp.getWriter();
        final ServletContext ctx = getServletContext();
        w.printf("Context path = %s\n", ctx.getContextPath());
        w.printf("Application title = %s\n", ctx.getInitParameter("title"));
        final ServletConfig cfg = getServletConfig();
        w.printf("Servlet name = %s\n", cfg.getServletName());
        w.printf("Servlet title = %s\n", cfg.getInitParameter("title"));
        //final File currDir = new File(".").getCanonicalFile();
        final File currDir = new File(ctx.getRealPath("."));
        w.printf("Current dir = %s\n", currDir);
        ctx.setAttribute("ctxCount", ++count);
        w.printf("Request count = %d\n", count);
    }
    
    @Override
    public void destroy(){
        System.out.println(getServletConfig().getServletName()+" stopped");
    }
}
