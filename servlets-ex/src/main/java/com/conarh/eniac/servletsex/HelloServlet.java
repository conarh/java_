/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conarh.eniac.servletsex;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Conarh
 */
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        final PrintWriter w = resp.getWriter();
        resp.setContentType("text/html");
        w.println("<html><body>");
        for (int i=1; i<=80; i++){
            w.printf("%d\n", i);
            if ((i % 40) == 0){
                w.println("<br>");
            }
        }
        w.printf("<p>Number of ContextInfoServlet requests: %d</p>", getServletContext().getAttribute("ctxCount"));
        w.println("</body></html>");
        //resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    

}
