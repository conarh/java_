/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author Конарх
 */
public class MessageController extends AbstractController{

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        hsr.setCharacterEncoding("UTF-8");
        final String mess = hsr.getParameter("message").trim();
        final ModelAndView reply = new ModelAndView("message");
        if(!mess.isEmpty()){
            reply.addObject("message", mess);
        }
        return reply;
    }
    
}
