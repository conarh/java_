/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.annotated;

import com.gwain.springexgwain.beans.SearcherBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author Конарх
 */
@Component("annIndexController")
@Scope("singleton")
public class AnnotatedIndexController extends AbstractController{
    @Autowired
    private AnnotatedSearcherBean searcher;
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        System.out.println(String.format("Index controller called"));
        final ModelAndView reply = new ModelAndView();
        reply.setViewName("index");
        reply.addObject("searchable", searcher.getSearchable());
        return reply;
    }

}
