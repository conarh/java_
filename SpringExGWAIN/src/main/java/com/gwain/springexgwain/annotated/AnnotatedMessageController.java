/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.annotated;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.gwain.springexgwain.entites.MessageBean;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Конарх
 */
@Controller("annMessage")
@Scope("session")
@SessionAttributes({"message2"})
public class AnnotatedMessageController {
    //private MessageBean mess = new MessageBean();//not multisession-safe
    
    @ModelAttribute("message2")
    public MessageBean getMessageBean(){
        //return mess;
        return new MessageBean();
    }
    
    @RequestMapping(method=RequestMethod.GET)
    public String get(){
        return "message";
    }
    
//    @RequestMapping(method=RequestMethod.POST)
//    public ModelAndView showMessage(@ModelAttribute MessageBean bean){
//        mess.setMessage(bean.getMessage());
//        final ModelAndView model = new ModelAndView("message");
//        model.addObject("message", bean.getMessage());
//        return model;
//    }
    
    @RequestMapping(method=RequestMethod.POST)
    public String showMessage(@ModelAttribute("message2") MessageBean bean, final SessionStatus status){
        //mess.setMessage(bean.getMessage());
        status.setComplete();
        return "message";
    }
}
