/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.annotated;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Конарх
 */
@Controller("annIndexController2")
public class AnnotatedIndexController2 {
    @Autowired
    private AnnotatedSearcherBean searcher;
    
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView get(){
        final ModelAndView reply = new ModelAndView();
        reply.setViewName("index");
        reply.addObject("searchable", searcher.getSearchable());
        return reply;
    }
}
