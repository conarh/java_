<%-- 
    Document   : message
    Created on : Jun 20, 2013, 10:26:29 PM
    Author     : Конарх
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Message Page from Spring</title>
    </head>
    <body>
        <h1>Spring controllers!</h1>
        <!--Last entered message-->
        <c:out value="${message}"/>
        <c:out value="${message2.message}"/>
        <hr>
        <form method="post" action="message.html">
            <input type="text" name="message"><br>
            <input type="submit" value="Добавить">
        </form>
    </body>
</html>
