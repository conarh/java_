/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsex;

/**
 *
 * @author Конарх
 */
public class MutableItem {
    private int val;
    
    public MutableItem(int val){
        setVal(val);
    }
    
    public void setVal(int val){
        this.val = val;
    }
    
    public int getVal(){
        return val;
    }
    
    @Override
    public String toString(){
        return "m"+val;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = 31 * hash + this.val;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MutableItem other = (MutableItem) obj;
        return val==other.val;
    }
    
}
