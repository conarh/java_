/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author Конарх
 */
public class CollectionsExMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Set col = new HashSet(4, 0.75f);
        for (int i=0; i<10; i++){
            col.add(new Item(i));
        }
        for (Iterator itr = col.iterator(); itr.hasNext(); ){
            Item val = (Item)itr.next();
            /*if (val.getVal()==2){
                col.remove(val);
                itr = col.iterator();
            }*/
            if (val.getVal()==2) itr.remove();
            System.out.printf("%s, ", val);
        }
        System.out.println();
        System.out.println(col);
        //Object[] arr = col.toArray();
        Item[] origArr = new Item[col.size()-1];
        Item[] arr = (Item[]) col.toArray(origArr);
        System.out.printf("%s %s\n", Arrays.toString(arr), origArr==arr);
        Item[] arr2 = (Item[]) col.toArray(new Item[col.size()]);
        System.out.println(Arrays.toString(arr2));
        final Set col2 = new HashSet();
        MutableItem oldMi5 = null;
        for (int i=0; i<10; i++){
            MutableItem  mi = new MutableItem(i);
            if (i==5) oldMi5 = mi;
            col2.add(mi);
        }
        oldMi5.setVal(15);
        System.out.println(col2);
        //oldMi5.setVal(3);
        //final MutableItem mi5 = new MutableItem(5);
        final MutableItem mi5 = new MutableItem(15);
        col2.remove(mi5);
        System.out.println(col2);
        System.out.printf("oldMi5 %d == mi5 %d, equals=%s\n", oldMi5.hashCode(), mi5.hashCode(), oldMi5.equals(mi5));
        List lst = new ArrayList(col);
        for (int i=0; i<lst.size(); i++){
            System.out.printf("%s, ", lst.get(i));
        }
        System.out.println();
        lst.set(4, new Item(25));
        lst.add(4, new Item(25));
        System.out.println(lst);
        List sub = lst.subList(3, 7);
        System.out.println(sub);
        sub.remove(1);
        System.out.printf("%s\n%s\n", sub, lst);
        Vector v = new Vector();
        v.addAll(lst);
        for (Enumeration en = v.elements(); en.hasMoreElements();){
            System.out.printf("%s, ", en.nextElement());
        }
        System.out.println();
        v.setSize(25);
        System.out.println(v);
        final Vector v2 = new Vector();
        //v2.ensureCapacity(60);
        for (int i=0; i<100; i++){
            v2.add(i);
            System.out.printf("size=%d capacity=%d\n", v2.size(), v2.capacity());
        }
        v2.trimToSize();
        System.out.printf("size=%d capacity=%d\n", v2.size(), v2.capacity());
    }
    
    public static int giveSome(){
        throw new UnsupportedOperationException(":(");
    }
    
    public static void doSome(){
        throw new UnsupportedOperationException(":(");
    }
}
