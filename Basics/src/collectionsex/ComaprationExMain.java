/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsex;

import java.util.Comparator;
import java.util.Random;
import java.util.TreeSet;

/**
 *
 * @author Конарх
 */
public class ComaprationExMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final TreeSet sorted = new TreeSet(new Comparator(){

            @Override
            public int compare(Object o1, Object o2) {
                if (o1.equals(o2)) {
                    return 0;
                } else {
                    SortableItem i1 = (SortableItem) o1;
                    SortableItem i2 = (SortableItem) o2;
                    if (i1.get() < i2.get()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
        });
        final Random rnd = new Random();
        for (int i=0; i<50; i++){
            sorted.add(new SortableItem(rnd.nextInt(2000)));
        }
        System.out.println(sorted);
    }
}
