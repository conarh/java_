/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsex;

/**
 *
 * @author Конарх
 */
public class Item {
    private int val;
    
    public Item(int val){
        this.val = val;
    }
    
    public int getVal(){
        return val;
    }
    
    @Override
    public String toString(){
        return Integer.toString(val);
    }
}
