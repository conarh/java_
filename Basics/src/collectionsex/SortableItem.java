/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsex;

/**
 *
 * @author Конарх
 */
public class SortableItem /*implements Comparable*/{
    private int val;
    
    public SortableItem(int val){
        this.val = val;
    }
    
    public int get(){
        return val;
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null){
            return false;
        }
        if (o == this){
            return true;
        }
        if (o.getClass() == SortableItem.class){
            if (val == ((SortableItem)o).val) return true;
        }
        return false;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int hash = 1;
        hash = prime*hash + val;
        return hash;
    }
    
    @Override
    public String toString(){
        return Integer.toString(val);
    }

    /*@Override
    public int compareTo(Object o) {
        if (equals(o)){
            return 0;
        }
        final SortableItem item = (SortableItem)o;
        if (val < item.val){
            return -1;
        } else {
            return 1;
        }
    }*/
}
