package stat;

public class StatChild extends StatParent{
	
	static {
		System.out.println("Child init");
		if (true){
			throw new IllegalStateException("Exception in static initialization");
		}
	}
	
	public static void doSome(){
		System.out.println("Child");
	}
}
