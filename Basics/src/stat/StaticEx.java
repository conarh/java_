/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package stat;

/**
 *
 * @author Конарх
 */
public class StaticEx {
    private int count;
    private static int statCount;
    
    static {
        statCount = 1;
    }
    
    public StaticEx(){
        count++;
        statCount++;
        System.out.printf("count=%d, static count=%d\n", count, statCount);
    }
    
    public static void main(String[] args){
        StaticEx.printStat();
        for (int i=0; i<10; i++){
            new StaticEx();
        }
        //Static parent and child call
        StatParent.doSome();
//        Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//			
//			@Override
//			public void uncaughtException(Thread t, Throwable e) {
//				System.out.printf("%s: %s\n", t, e);
//			}
//		});
        
        try{
        	StatChild.doSome();
        } catch (Error ex){
        	System.out.printf("Exception: %s\n", ex);
        }
        StatChild.doSome();
        System.out.println("Continue main method");
    }
    
    public static void printStat(){
        System.out.printf("Initial static=%d\n", StaticEx.statCount);
        /*StaticEx sex = new StaticEx();
        sex.count=4;*/
    }
}
