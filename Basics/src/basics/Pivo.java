/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Конарх
 */
public class Pivo {
    
    public static void main(String[] args){
        final Random rnd = new Random();
        final double[] prices = new double[3];
        for (int i=0; i<prices.length; i++){
            prices[i] = (rnd.nextInt(1301)+700)/100.0;
        }
        final int[][] pivo = new int[20][prices.length];
        for (int i=0; i<pivo.length; i++){
            for (int j=0; j<pivo[i].length; j++){
                pivo[i][j] = rnd.nextInt(20);
            }
        }
        double gCount = 0;
        final double[] sorts = new double[prices.length];
        for (int i=0; i<pivo.length; i++){
            double count = 0;
            for (int j=0; j<pivo[i].length; j++){
                final double sortCost=pivo[i][j]*prices[j];
                sorts[j] += sortCost;
                count+=sortCost;
            }
            System.out.println(String.format("Alconaft #%d summa=%.2f", i+1, count));
            gCount+=count;
        }
        System.out.println(String.format("Sort costs: %s\nTotlally: %.2f", Arrays.toString(sorts), gCount));
    }
}
