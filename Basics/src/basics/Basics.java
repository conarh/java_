/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

import java.util.Arrays;

/**
 *
 * @author Конарх
 */
public class Basics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        byte b = 34;//1 byte
        short sh = 3453;//2 bytes
        int v = 1234213524;//4 bytes
        long l = 2341252352315325231L;//8 bytes
        l = v;
        v = (int) l;
        float f = 23423424.234234234f;//4 bytes
        double d = 32424.234234234234234245;//8 bytes
        char ch = 'r';//2 bytes
        v = ch;
        ch = (char) v;
        boolean bool = false;//1 byte
        String str = "sdfseagqgqerbaerbere";
        str = "Slava "+" robotam"+v+", ubit vseh ludey!";
        str = String.format("Slava robotam %s ubit vseh ludey", v);
        str = ""+(2+3);
        System.out.println(str);
        double res = 6/11*100;
        System.out.println(res);
        v = 0xFF00BA03;
        v = 045;
        b = -113;
        int signedByte = (int)b;
        int unsignedByte = b&0xFF;
        System.out.println(String.format("Signed=%d unsigned=%d", signedByte, unsignedByte));
        int a=1;
        a += a++ + a + ++a + a-- - --a;
        System.out.println(a);
        a = 3;
        boolean chet = a%2==0;
        //== <= < > >= !=
        /* 
         && || ! & | ! ^
         */
        a = 2;
        a = a<<3;
        System.out.println(a);
        if (a>4){
            System.out.println("a>4");
        } else 
        if (a==4){System.out.println("a==4");
            System.out.println("a==4");
        } else {
            System.out.println("a<4");
        }
        if (a>4) 
            System.out.println("a>4");
        else 
            System.out.println("a<=4");
        boolean moreThenFor = a>4;
        System.out.println(String.format("a%s4", a>4 ? ">" : "<"));
        String val = "Conarh likes KISS";
        if (val!=null&&val.length()>6){
            System.out.println(val.substring(0, 6)+"...");
        } else {
            System.out.println(val);
        }
        System.out.println(val!=null ? String.format("%s%s", 
                val.substring(0, Math.min(val.length(),6)),
                val.length()>6 ? "..." : "") : null);
        int value = 2;
        {
            int someVal = 34+value;
        }
        //value = someVal;
        boolean someVal = true;
        int weekday = 10;
        switch (weekday){
            case 1: 
                System.out.println("Mon");
                break;
            case 5: 
                System.out.println("Fri");
                break;
            default:
                System.out.println("0_o");
        }
        int count = 0;
        while (++count<=5){
            System.out.println(count+"...");
        }
        System.out.println("Я иду искать");
        count = 7;
        do {
            System.out.println(count+"...");
        } while(++count<=5);
        for (int i=1; i<=5; i++){
            //if (i==3) break;
            if (i==3) continue;
            System.out.println(i+"...");
        }
        /*for (;;){
            
        }*/
        int[] arr = new int[10];
        //arr = null;
        for (int i=0; i<arr.length; i++){
            arr[i] = i+1;
        }
        System.out.println(Arrays.toString(arr));
        int[][] arr2 = new int[10][4];
        int[] subArr = arr2[3];
        arr2[3] = new int[10];
        arr2[4] = new int[]{23, 424, 23};
        int[] subArr2 = {23, 234, 234, 123};
        long start = System.currentTimeMillis();
        int[] src = new int[1024];
        int[] tgt = null;
        for (int i=0; i<1000000; i++){
            Arrays.fill(src, 666);
            tgt = new int[src.length];
            /*int index = 0;
            for (int arrVal : src){
                tgt[index++] = arrVal;
            }*/
            System.arraycopy(src, 0, tgt, 0, tgt.length);
        }
        long time = System.currentTimeMillis()-start;
        System.out.println("Copy time = "+time);
    }
    
}
        