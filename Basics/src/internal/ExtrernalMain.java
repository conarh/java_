/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package internal;

import internal.External;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Конарх
 */
public class ExtrernalMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            External.ABHolder holder = new External.ABHolder();
            holder.a = 10;
            holder.b = 13;
            final External ext = new External(holder);
            holder = ext.getHolder();
            System.out.printf("a=%d b=%d\n", holder.a, holder.b);
            holder.a = 5;
            holder = ext.getHolder();
            System.out.printf("a=%d b=%d\n", holder.a, holder.b);
            External.Pavlik p = ext.getPavlik();
            p.setA(34);
            holder = ext.getHolder();
            System.out.printf("a=%d b=%d\n", holder.a, holder.b);
            p = ext.getShtirlits();
            p.setA(34);
            holder = ext.getHolder();
            System.out.printf("a=%d b=%d\n", holder.a, holder.b);
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
    }
}
