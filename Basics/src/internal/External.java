/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package internal;

/**
 *
 * @author Конарх
 */
public class External {
    public ABHolder holder = new ABHolder();
    
    public static class ABHolder implements Cloneable{
        public int a;
        public int b;
        
        public ABHolder clone() throws CloneNotSupportedException{
            //holder.a = 0;//нельзя - статик
            return (ABHolder)super.clone();
        }
    }
    
    public class Pavlik{

        public void setA(int a){
            holder.a = a;
        }
    }
    
    public External(ABHolder holder){
        this.holder.a = holder.a;
        this.holder.b = holder.b;
    }
    
    public int getA(){
        return holder.a;
    }
    
    public int getB(){
        return holder.b;
    }
    
    public ABHolder getHolder() throws CloneNotSupportedException{
        return holder.clone();
    }
    
    public Pavlik getPavlik(){
        return new Pavlik();
    }
    
    public Pavlik getShtirlits(){
        final int val = 1945;
        class Shtirlits extends Pavlik{
        
            public void setA(int a){
                External.this.holder.a = val;
            }
        
        }
        return new Shtirlits();
    }
}
