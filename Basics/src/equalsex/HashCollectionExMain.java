/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package equalsex;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Conarh
 */
public class HashCollectionExMain {
    
    public static void main(String[] args){
        final Set<Item> c = new HashSet<>();
        System.out.printf("Initial size = %d\n", c.size());
        final Item a = new Item("A");
        c.add(a);
        System.out.printf("First insert size = %d, hash = %d\n", c.size(), a.hashCode());
        a.setName("B");
//        c.add(a);
//        System.out.printf("Second insert size = %d\n, hash = %d", c.size(), a.hashCode());
        c.remove(a);
        System.out.printf("After remove size = %d, hash = %d\n", c.size(), a.hashCode());
    }
}
