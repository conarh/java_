/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package equalsex;

/**
 *
 * @author Конарх
 */
public class EqualsExMain_ {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Object o1 = new Object();
        Object o2 = new Object();
        Object o3 = o1;
        compare(o1, o2);
        compare(o1, o3);
        System.out.println("----------------------------------------------");
        Item i1 = new Item("Item 1");
        Item i2 = new Item("Item 2");
        Item i3 = new Item("Item 1");
        compare(i1, i2);
        compare(i1, i3);
        System.out.println("----------------------------------------------");
        String s1 = "String 1";
        String s2 = "String 2";
        String s3 = "String 1";
        compare(s1, s2);
        compare(s1, s3);
        String s4 = new String(new char[]{'S', 't', 'r', 'i', 'n', 'g', ' ', '2'});
        System.out.println("----------------------------------------------");
        compare (s2, s4);
        s4 = s4.intern();
        compare (s2, s4);
    }
    
    public static void compare(final Object o1, final Object o2){
        System.out.println(String.format("%s == %s \t%b",o1, o2, o1==o2));
        System.out.println(String.format("%s equals %s \t%b",o1, o2, o1.equals(o2)));
        System.out.println(String.format("%s hash = %d, %s hash = %d", o1, o1.hashCode(), o2, o2.hashCode()));
    }
}


