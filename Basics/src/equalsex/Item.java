/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package equalsex;

/**
 *
 * @author Конарх
 */
public class Item {
    private String name;
    
    public Item(final String name){
        this.name = name;
    }
    
    //NEVER call this when object is already inside a hash collection, because this will affect hashcode
    public void setName(final String name){
        this.name = name;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
    @Override
    public boolean equals(final Object o){
        if (o==null) return false;
        if (o==this) return true;
        if (getClass()==o.getClass()){
            return name.equals(((Item)o).name);
        } else return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 1;
        hash = hash*prime+(name==null ? 0 : name.hashCode());
        return hash;
    }
    
    
}
