/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package equalsex;

/**
 *
 * @author Конарх
 */
public class SomeSingleton {
    private static SomeSingleton inst;
    
    private SomeSingleton(){
        
    }

    public static SomeSingleton instance(){
        if (inst==null){
            inst = new SomeSingleton();
        }
        return inst;
    }
}
