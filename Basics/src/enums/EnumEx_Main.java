/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package enums;

import java.util.Random;

/**
 *
 * @author Конарх
 */
public class EnumEx_Main {
    private static enum WeekDays{
        MON("Понедельник"){
            @Override
            public void printWeekend(){
                System.out.println("Работайте негры, солнце еще высоко");
            }
        },
        TUE("Вторник"){
            @Override
            public void printWeekend(){
                System.out.println("Работайте негры, солнце еще высоко");
            }
        },
        FRI("Пятница"){
            @Override
            public void printWeekend(){
                System.out.println("Уррррраааааааа!!!!!!! Тяпница");
            }
        },
        SUN("Воскресенье"){
            @Override
            public void printWeekend(){
                System.out.println("САН");
            }
        };
        
        private String name;
        
        private WeekDays(String name){
            this.name = name;
        }
        
        public WeekDays before(){
            if (this.ordinal()>0){
                return values()[ordinal()-1];
            } else {
                throw new IllegalArgumentException(String.format("No day before %s", this.name));
            }
        }
        
        public abstract void printWeekend();
        
        @Override
        public String toString(){
            return name;
        }
    }
    
    private static class WeekDaysAnalog{
        public static final WeekDaysAnalog MON = new WeekDaysAnalog();
        public static final WeekDaysAnalog TUE = new WeekDaysAnalog();
        public static final WeekDaysAnalog FRI = new WeekDaysAnalog();
        public static final WeekDaysAnalog SUN = new WeekDaysAnalog();
        
        private WeekDaysAnalog(){}
    }
    
    public static void main(String[] args){
        Random rnd = new Random();
        WeekDays day = WeekDays.values()[rnd.nextInt(WeekDays.values().length)];
        System.out.println(day.name());
        //Using switch
        switch (day){
            case MON: 
                System.out.println("Понедельник");
                break;
            case TUE:
                System.out.println("Вторник");
                break;
            case FRI:
                System.out.println("Пятница");
                break;
            case SUN:
                System.out.println("Воскресенье");
                break;
        }
        System.out.println(day);
        System.out.printf("Before %s: %s\n", day, day.before());
    }
}
