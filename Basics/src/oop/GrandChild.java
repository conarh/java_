/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

/**
 *
 * @author Конарх
 */
public class GrandChild extends Child{

    public GrandChild() {
        super(false);
        System.out.println("GrandChild");
    }
    
    {
        System.out.println("block 1");
    }
    
    {
        System.out.println("block 2");
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new GrandChild();
    }
}
