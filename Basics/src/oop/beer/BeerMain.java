/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oop.beer;

import java.util.Random;

/**
 *
 * @author Конарх
 */
public class BeerMain {

    public static void main(String[] args) {
        final double[] prices = new double[3];
        final Random rnd = new Random();
        for (int i=0; i<prices.length; i++){
            prices[i] = (rnd.nextInt(3001)+1000)/100.0;
        }
        final Buyer[] buyers = new Buyer[20];
        for (int i=0; i<buyers.length; i++){
            final Buyer b = new Buyer(prices);
            while (!b.isFull()){
                b.addSort(rnd.nextInt(20));
            }
            buyers[i] = b;
        }
        double totalBuyers = 0;
        for (Buyer b : buyers){
            final double cost = b.getCost();
            totalBuyers = cost;
            System.out.println(String.format("buyer: cost=%.2f", cost));
        }
        System.out.println(String.format("Total buyer = %.2f", totalBuyers));
    }
}
