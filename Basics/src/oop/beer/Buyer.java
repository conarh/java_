/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oop.beer;

/**
 *
 * @author Конарх
 */
public class Buyer {
    private final int[] amounts;
    private int index = 0;
    private final double[] prices;
    
    public Buyer(final double[] prices){
        amounts = new int[prices.length];
        this.prices = prices;
    }
    
    public void addSort(final int amount){
        amounts[index++] = amount;
    }
    
    public boolean isFull(){
        return index>=amounts.length;
    }
    
    public double getCost(){
        double count = 0;
        for (int i=0; i<amounts.length; i++){
            count+=getCost(i);
        }
        return count;
    }
    
    public double getCost(int index){
        return amounts[index]*prices[index];
    }
}
