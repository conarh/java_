/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oop.mylist;

/**
 *
 * @author Конарх
 */
public class MyList {
    private final int index;
    private int value = 0;
    private MyList next = null;
    
    private MyList(int index){
        this.index = index;
    }
    
    public MyList(){
        this(0);
    }
    
    public void add(int index, int value){
        if (index==this.index){
            this.value = value;
        } else {
            if (next==null){
                next = new MyList(this.index+1);
            }
            next.add(index, value);
        }
    }
    
    public int get(int index){
       if (this.index==index){
           return value;
       } else {
           if (next==null){
               throw new ArrayIndexOutOfBoundsException(index);
           } else {
               return next.get(index);
           }
       }
    }
    
    public int size(){
        return size(0);
    }
    
    private int size(int previous){
        previous++;
        return next==null ? previous : next.size(previous);
    }
}
