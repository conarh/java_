/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oop.mylist;

/**
 *
 * @author Конарх
 */
public class MyListMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyList list = new MyList();
        for (int i=0; i<10; i++){
            list.add(i, i+1);
        }
        for (int i=list.size()-1; i>=0; i--){
            System.out.println(i+"="+list.get(i));
        }
    }
}
