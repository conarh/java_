/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author Конарх
 */
public class PropertyChangeEx {
     private static class Ticker {
        PropertyChangeSupport pcs = new PropertyChangeSupport(this);
         
        public void count() {
            pcs.firePropertyChange("started", false, true);
            for (int i = 0; i < 10; i++) {
                pcs.firePropertyChange("tick", i-1, i);
            }
            pcs.firePropertyChange("started", true, false);
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
            pcs.addPropertyChangeListener(listener);
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
            pcs.removePropertyChangeListener(listener);
        }

        public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            pcs.addPropertyChangeListener(propertyName, listener);
        }
       
    };

    public static void main(String[] args) {
        Ticker t = new Ticker();
        /*t.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("started")){
                    System.out.printf("Ticker %s\n", (Boolean)evt.getNewValue() ? "started" : "stopped");
                } else 
                if ("tick".equals(evt.getPropertyName())) {
                    System.out.printf("result %s\n", evt.getNewValue());
                }
            }
        });*/
        t.addPropertyChangeListener("started", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.out.printf("Ticker %s\n", (Boolean)evt.getNewValue() ? "started" : "stopped");
            }
        });
        t.addPropertyChangeListener("tick", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.out.printf("result %s\n", evt.getNewValue());
            }
        });
        t.count();
    }
}
