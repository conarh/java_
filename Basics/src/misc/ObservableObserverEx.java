/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Конарх
 */
public class ObservableObserverEx {
    private static class Ticker extends Observable{
            
            public void count(){
                for (int i=0; i<10; i++){
                    setChanged();
                    notifyObservers(i);
                }
            }
            
        };
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Ticker t = new Ticker();
        t.addObserver(new Observer(){

            @Override
            public void update(Observable o, Object arg) {
                System.out.printf("result %s\n", arg);
            }
            
        });
        t.count();
    }
}
