/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package volatileex;

/**
 *
 * @author Конарх
 */
public class LongVolatileMain {
    public /*volatile*/ long val = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            final LongVolatileMain v = new LongVolatileMain();
            Thread t1 = new Thread(){
                public void run(){
                    for (int i=0; true; i++){
                        v.val = Math.random()<0.5 ? Long.MIN_VALUE : Long.MAX_VALUE;
                    }
                }
            };
            Thread t2 = new Thread(){
                private int count=0;

                public void run(){
                    while (true){
                        long k = v.val;
                        if (k!=Long.MIN_VALUE&&k!=Long.MAX_VALUE&&k!=0){
                            System.out.println((++count)+": "+k);
                        }
                    }
                }
            };
            t2.setDaemon(true);
            t1.start();
            t2.start();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
