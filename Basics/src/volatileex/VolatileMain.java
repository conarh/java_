package volatileex;

public class VolatileMain {
	private long l;
	
	public static void main(String[] args) {
		new VolatileMain();
	}
	
	public VolatileMain(){
		Thread t = new Thread(){
			private int count = 0;

			@Override
			public void run() {
				while (true) {
					if (++count % 2 == 0) {
						l = Long.MIN_VALUE;
					} else {
						l = Long.MAX_VALUE;
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
		System.out.printf("Min value = %s\n", Long.MIN_VALUE);
		System.out.printf("Max value = %s\n", Long.MAX_VALUE);
		while (true){
			long k = l;
			if (k != Long.MIN_VALUE && k != Long.MAX_VALUE){
				System.out.println(k);
			}
		}
	}

}
