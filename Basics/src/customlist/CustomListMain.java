/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package customlist;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Конарх
 */
public class CustomListMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List l = new CustomList();
        for (int i=0; i<10; i++){
            l.add(i);
        }
        for (int i=l.size()-1; i>=0; i--){
            System.out.printf("%d ", l.get(i));
        }
        System.out.println();
        l.remove((Object)3);
        for (Iterator itr=l.iterator(); itr.hasNext();){
            int value = (Integer)itr.next();
            System.out.printf("%d ", value);
            if (value==5) itr.remove();//l.add(66);
        }
        System.out.println();
        System.out.println(l);
    }
}
