/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package customlist;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Конарх
 */
public class CustomList implements List{
    private Object[] data = new Object[7];
    private int size = 0;
    private long version = 0;

    @Override
    public int size() {
        return size;
    }
    
    @Override
    public boolean add(Object e) {
        if (size>=data.length){
            Object[] tmp = data;
            data = new Object[tmp.length*2];
            System.arraycopy(tmp, 0, data, 0, tmp.length);
            System.out.printf("capacity %d\n", data.length);
        }
        data[size++] = e;
        version++;
        return true;
    }
    
    @Override
    public Object get(int index) {
        if ((index<0) || (index>=size())) throw new ArrayIndexOutOfBoundsException(index);
        return data[index];
    }

    @Override
    public Iterator iterator() {
        return new Iterator(){
            private int index = 0;
            private long version = CustomList.this.version;
            private boolean removed = true;
            
            @Override
            public boolean hasNext() {
                if (CustomList.this.version!=version) throw new java.util.ConcurrentModificationException();
                return index<size();
            }

            @Override
            public Object next() {
                try{
                    if (CustomList.this.version!=version) throw new java.util.ConcurrentModificationException();
                    removed = false;
                    return get(index++);
                } catch (ArrayIndexOutOfBoundsException ex){
                    throw new NoSuchElementException();
                }
            }

            @Override
            public void remove() {
                if (removed) throw new IllegalStateException();
                CustomList.this.remove(get(--index));
                version = CustomList.this.version;
                removed = true;
            }
            
        };
    }
    
    @Override
    public boolean remove(Object o) {
        for (int i=0; i<size(); i++){
            if (data[i].equals(o)){
                version++;
                for (int j=i+1; j<size; j++){
                    data[j-1] = data[j];
                }
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<size; i++){
            sb.append(String.format("%s ", data[i]));
        }
        return sb.toString();
    }
    
    
//-------------------------------------------------------------------
    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object[] toArray(Object[] arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object set(int index, Object element) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add(int index, Object element) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object remove(int index) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ListIterator listIterator() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ListIterator listIterator(int index) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
