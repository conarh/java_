/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Конарх
 */
public class Printer implements MyObserverInt, MyPrintableObserverInt{

    @Override
    public void fire(MyObservableInt src) {
        System.out.println(src);
    }

    @Override
    public String getDescrition() {
        return toString();
    }
    
}
