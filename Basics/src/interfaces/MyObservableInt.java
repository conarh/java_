/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Конарх
 */
public interface MyObservableInt {
    
    void addObserver(MyObserverInt observer);
    
    MyObserverInt getObserver();
    
    void doSome();
}
