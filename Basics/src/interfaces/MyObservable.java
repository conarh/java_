/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Конарх
 */
public class MyObservable {
    private MyObserverInt observer;
    
    public void addObserver(MyObserverInt observer){
        this.observer = observer;
    }
    
    protected MyObserverInt getObserver(){ 
        return observer;
    }
}
