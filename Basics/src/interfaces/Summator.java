/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Конарх
 */
public class Summator extends AbstractMyObservable{
    private int a;
    private int b;
    private int c;

    public void sum(int a, int b){
        this.a = a;
        this.b = b;
    }
    
    @Override
    public String toString(){
        return Integer.toString(c);
    }

    @Override
    public void doSome() {
        c = a+b;
        getObserver().fire(this);
    }
    
}
