/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Конарх
 */
public abstract class AbstractMyObservable implements MyObservableInt{
    private MyObserverInt observer;

    @Override
    public void addObserver(MyObserverInt observer) {
        this.observer = observer;
    }

    @Override
    public MyObserverInt getObserver() {
        return observer;
    }

    @Override
    public abstract void doSome();
    
}
