package concurrent.deadlock;

public class DeadlockMain {

	public static void main(String[] args) {
		Worker w1 = new Worker();
		w1.setName("t1");
		Worker w2 = new Worker();
		w2.setName("\tt2");
		w1.setCoworker(w2);
		w2.setCoworker(w1);
		w1.start();
		w2.start();
	}

}

class Worker extends Thread{
	private boolean busy = false;
	private Worker coworker;
	private int count = 0;
	
	public synchronized boolean isBusy() {
		return busy;
	}

	public void setCoworker(Worker coworker) {
		this.coworker = coworker;
	}

	public void run(){
		try {
			while (true) {
				synchronized (this) {
					busy = true;
				}
				Thread.sleep(1);
				System.out.printf("%s %d\n", getName(), count++);
				synchronized (this) {
					busy = false;
				}
				synchronized (coworker) {
					if (!coworker.isBusy()) {
						coworker.notify();
						synchronized (this) {
							this.wait(10);
						}
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}