package concurrent;

public class WaitNotifyMain {

	public static void main(String[] args) {
		final Object mon = new Object();
		final Thread t1 = new Thread(){
			public void run(){
				while (true){
					synchronized (mon){
						System.out.println("T1: before wait");
						try {
//							mon.wait(3000);//Wake by timeout
							mon.wait();
						} catch (InterruptedException e) {
							System.out.println("T1: interrupted");
						}
						System.out.println("T1: after wait");
					}
				}
			}
		};
		final Thread t2 = new Thread(){
			public void run(){
				while (true){
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					synchronized (mon){
						System.out.println("\tT2: before notify");
//						mon.notify();//Wake by notify
						t1.interrupt();//Wake by interrupt
						System.out.println("\tT2: after notify");
					}
				}
			}
		};
		t1.start();
		t2.start();
	}

}
