package exceptions;

public class NPEInIfExamlpeMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		final String a = "test";
		final String a = null;
		//Solution: put != null check at the begiinig of && condition
//		if ((a != null) && (a.length() > 3)){  
//			System.out.printf("Length of %s is bigger than 3\n" , a);
//		}
		final boolean showLength = false;
		System.out.println(showLength ? 
				String.format("Length of %s is %d\n" , a, a.length()) 
				: "");
	}

}
