package exceptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Конарх
 */
public class ExMain_ {

    public static void runtime(){
        PrintStream out = null;
        //out = System.out;
        out.println("Hello, world");
    }
    
    public static void error(){
        byte[] arr = new byte[1024*1024*500];
        //error();
    }
    
    public static void openFile() throws IOException{
        /*try {
            FileInputStream in = new FileInputStream("somefile.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }*/
        if (true) throw new UnsupportedOperationException(":(");
        FileInputStream in = new FileInputStream("somefile.txt");
        try{
            in.read();
        } finally{
            in.close();
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            //runtime();
            error();
            openFile();
        } catch (RuntimeException ex){
            System.out.println("runtime");
            ex.printStackTrace();
        } catch (Exception ex){
            System.out.println("exception");
            ex.printStackTrace();
        } finally {
            System.out.println("finally");
        }
    }
}
