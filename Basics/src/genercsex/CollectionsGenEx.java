/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package genercsex;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Конарх
 */
public class CollectionsGenEx {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> l1 = new ArrayList<String>();
        l1.add("String");
        l1.add("23");
        for (int i=0; i<l1.size(); i++){
            String val = (String) l1.get(i);
            System.out.println(val);
        }
        List<Number> l2 = new ArrayList<Number>();
        l2.add(new Integer(10));
        List<Integer> l3 = new ArrayList<Integer>();
        //l2 = l3;//Нельзя  - опасно с точки зрения приведения типов
        l2.add(new Double(34));
        /*if (l2 instanceof List){
            l2.add(34.56);
        }*/
        Number n = l2.get(1);
        Integer integ = (Integer) l2.get(0);
        List<?> any = l2;
        any = l3;
        any = l1;
        Object o = any.get(3);
        //any.add(3);//нельзя - вайлдкарта
        List<? extends Number> numeric = new ArrayList<Number>();
        numeric = l2;
        Number num = numeric.get(1);
        List<? super Integer> supernum = new ArrayList<Number>();
        Object integ2 = supernum.get(3);
        supernum.add(2);
        //List<Integer>[] arr = new ArrayList<Integer>[10];
        //Object obj = arr;
        //List<String>[] strArr = (List<String>[])obj;
        
    }
    
    public static void print(List<?> obj){
        for (int i=0; i<obj.size(); i++){
            System.out.printf("%s ", obj.get(i));
        }
        System.out.println();
    }
}
