/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioexample_gwain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Конарх
 */
public class EncodingIOMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String someText = "Some text Какой-то текст 一部のテキスト";
        final File textFile = new File("textfile.txt");
        Writer out = null;
        BufferedReader in = null;
        try {
            out = new OutputStreamWriter(new FileOutputStream(textFile), "utf-16");
            out.write(someText);
            out.close();
            in = new BufferedReader(new InputStreamReader(new FileInputStream(textFile), "utf-16"));
            while (in.ready()){
                System.out.println(in.readLine());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out!=null){
                try {
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (in!=null){
                try {
                    in.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
