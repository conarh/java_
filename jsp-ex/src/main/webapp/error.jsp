<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ERROR!</title>
    </head>
    <body bgcolor="#ff0000">
        <h1>
        <font color="#ffffff">
            <%=exception.getMessage() %>
        </font>
        </h1>
        <a href="login.jsp">login</a>
    </body>
</html>
