<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" info="Sample JSP Page"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Random" %>

<%! 
private Random grnd = new Random();
private int max = 1;
%>

<%!
@Override
public void init(){
    max = 10;
}

@Override
public void doGet(HttpServletRequest req, HttpServletResponse resp){
    System.out.printf("%s - called\n", getServletInfo());
}
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <a href="login.jsp">login</a>
<%!
private int getRandom(){
    return grnd.nextInt(max);
}
%>
        <% Random rnd = new Random(); %>
        <ul>
            <% for (int i=0; i<10; i++){ 
                int val = rnd.nextInt(10);
                pageContext.setAttribute("value", val);
            %>
            <%-- <li><%= rnd.nextInt(10)%> --%>
            <%-- <li><%= val%> --%>
            <%--<li>${value < 5}--%>
            <%--<li>${value}--%>
            <li><%= getRandom()%>
            <% } %>
        </ul>
    </body>
</html>
