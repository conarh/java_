<%-- 
    Document   : protrected
    Created on : Dec 14, 2013, 1:16:46 PM
    Author     : Conarh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="error.jsp"%>
<!DOCTYPE html>
<%@include file="WEB-INF/jspf/check.jspf" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Protected page</title>
    </head>
    <body>
        <h2>Hello, ${user}</h2>
        <a href="login.jsp">login</a>
    </body>
</html>
