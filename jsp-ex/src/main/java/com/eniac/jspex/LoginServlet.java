/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eniac.jspex;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Login servlet
 * 
 * @author Conarh
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login.do"}, initParams = {
    @WebInitParam(name = "conarh", value="111111"),
    @WebInitParam(name = "bobo", value="666")
})
public class LoginServlet extends HttpServlet {
    private final Map<String, String> users = new HashMap<String, String>();
    
    public void init(){
//        for (Enumeration<String> en = getInitParameterNames(); en.hasMoreElements();){
//            final String name = en.nextElement();
//            //...
//        }
        for (String name : Collections.list(getInitParameterNames())){
            users.put(name, getInitParameter(name));
        }
        System.out.printf("%d users found\n", users.size());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        final String name = req.getParameter("login");
        final String password = req.getParameter("password");
//        if (password.equals(users.get(name))){
//            getServletContext().setAttribute("user", name);
//            resp.sendRedirect("protected.jsp");
//        } else {
//            throw new IllegalStateException("Invalid login or password");
//        }
        if (password.equals(users.get(name))){
            req.getSession().setAttribute("user", name);
        } else {
            req.getSession().removeAttribute("user");
        }
        //resp.sendRedirect("protected/protected2.jsp");
        resp.sendRedirect(resp.encodeRedirectURL("protected/protected2.jsp"));
    }
    
}
