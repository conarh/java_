/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eniac.servlets.advanced;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Conarh
 */
//@WebFilter(filterName = "AuthFilter", urlPatterns = {"/protected/*"}, dispatcherTypes = {DispatcherType.REQUEST})
public class AuthFilter implements Filter {
    private FilterConfig cfg;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Auth filter installed");
        cfg = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse resp = (HttpServletResponse) response;
        System.out.printf("Filter called BEFORE request %s\n", req.getRequestURI());
        final HttpSession sess = req.getSession(false);
        if ((sess == null) || (sess.getAttribute("user") == null)){
            resp.sendRedirect(String.format("%s/login2.jsp", cfg.getServletContext().getContextPath()));
//            final RequestDispatcher rd = cfg.getServletContext().getRequestDispatcher("/login2.jsp");
//            rd.forward(request, response);
        } else {
            chain.doFilter(request, response);
            System.out.printf("Filter called AFTER request %s\n", req.getRequestURI());
        }
    }

    @Override
    public void destroy() {
        
    }
    
}
