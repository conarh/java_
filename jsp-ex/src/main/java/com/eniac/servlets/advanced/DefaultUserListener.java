/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eniac.servlets.advanced;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener()
public class DefaultUserListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("---------------------Context created---------------------------");
        //sce.getServletContext().setAttribute("user", "conarh");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("---------------------Context destoryed--------------------------");
    }
}
