package com.sugar.servlets;

import com.sugar.users.SystemUsers;
import com.sugar.users.User;
import com.sugar.users.UserImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        final SystemUsers sysUsers = (SystemUsers) request.getServletContext().getAttribute ("sysUsers");
        final User user = (User) request.getSession().getAttribute ("user");
        
        final String login = (String) request.getParameter("login");
        final String password = (String) request.getParameter("password");
        
        final UserImpl adminUser = (UserImpl) request.getServletContext().getAttribute ("adminUser");
        if (true == adminUser.getLogin().equals(login) && true == adminUser.getPassword().equals(password)) {
            user.setLogin (login);
            user.setPassword (password);
            user.setIsAdmin (true);

            response.sendRedirect("protected/index.jsp");
        } else {
            final User sysUser = sysUsers.getUserByLogin(login);
            if (null == sysUser) {
                response.sendRedirect("protected/registration.jsp");
            } else {
                if (true == sysUser.getLogin().equals(login) && true == sysUser.getPassword().equals(password)) {
                    user.setLogin (login);
                    user.setPassword (password);
                    user.setIsAdmin (false);

                    response.sendRedirect("protected/index.jsp");
                } else {
                    response.sendRedirect("protected/registration.jsp");
                }
            }
        }
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo() {
        return "Login Servlet";
    }
    
}