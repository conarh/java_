package com.sugar.filters;

import com.sugar.users.User;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

public class AdminPanelFilter implements Filter {
    
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse resp = (HttpServletResponse) response;
        
        final User user = (User) req.getSession().getAttribute ("user");
        
        if (true == user.isNewUser()) {
            req.getSession().setAttribute("errorText", "An access is denied. Login to the system first.");
            resp.sendRedirect("../error.jsp");
        } else {
            if (false == user.isAdmin()) {
                req.getSession().setAttribute("errorText", "This user is not allowed to visit this restricted area.");
                resp.sendRedirect("../error.jsp");
            } else {
                chain.doFilter (request, response);
            }
        }
    }

    public void destroy() {        
    }

    @Override
    public void init (FilterConfig filterConfig) throws ServletException {
    }
    
}