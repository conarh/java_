package com.sugar.galery;

import java.util.Random;

/**
 *
 * @author LordNighton
 *
 */

public class RandomGaleryImagesFetcher {

    public static String fetchRandomImageName () {
        Random random = new Random();
        int randomNumber = random.nextInt(5) + 1;
        
        return String.format("randomgalery/%s.jpg", randomNumber);
    }
    
}