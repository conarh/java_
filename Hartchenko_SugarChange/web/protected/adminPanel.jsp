<%@page import="com.sugar.users.User"%>
<%@page import="java.util.List"%>
<%@page import="com.sugar.users.UserImpl"%>
<% String title = "Change Sugar Here - admin panel"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript">
            $(document).ready ( function() {
                $("#backButton").on('click', function (e) {
                    window.location.replace("index.jsp");
                });
                $("#backButton").on('mouseover', function (e) {
                    $(this).css ("background-color", "#DDD");
                });
                $("#backButton").on('mouseout', function (e) {
                    $(this).css ("background-color", "white");
                });
                
                $("img[id^='delete@']").on( 'click', function (e){
                    var id = $(this).attr ("id");
                    var userLogin = id.substring(7);
                    
                    $('input[type="submit"][value=' + userLogin + ']').click();
                });
            });
        </script>
    </head>
    <body>
        <div style='position : absolute; float : left'>
            <img id='backButton' style='width : 50px; border : 2px dashed #AAA; padding : 15px' src='../images/back.png'>
        </div>
        <center>
<%
            if (null != user && true == user.isAdmin()) {
%>
                <h2>Admin user(s)</h2>
                <table class="admintable" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><b>Login</b></td><td><b>Password</b></td>
                    </tr>    
                    <tr>
                        <td>
                            <%= adminUser.getLogin() %>
                        </td>
                        <td>
                            <%= adminUser.getPassword() %>
                        </td>
                    </tr>
                </table>
<%
            }
%>
            <br /><br />
<%                
            if (null != sysUsers && false == sysUsers.getUsers().isEmpty()) {
%>
                <h2>Simple user(s)</h2>
                <form method="post" action="../DeleteUserServlet">
                    <table class='admintable' cellpadding="0" cellspacing="0">
                        <tr style='font-weight : bold'>
                            <td>Login</td>
                            <td>Password</td>
                            <td>Actions</td>
                            <td style='width : 1px'></td>
                        </tr>    
<%
                List <User> usersList = sysUsers.getUsers();
                for (User next : usersList) {
%>
                        <tr>
                            <td>
                                <%= next.getLogin() %>
                            </td>
                            <td>
                                <%= next.getPassword() %>
                            </td>
                            <td>
<%
                                String id = "delete@" + next.getLogin();
%>
                                <img style='width : 25px' src="../images/delete.png" id='<%=id%>' />
                            </td>
                            <td>
                                <input style="visibility : hidden; width : 1px" name="delete" type='submit' value='<%= next.getLogin()%>' />
                            </td>
                        </tr>
<%
                }
%>
                    </table>
                </form>
<%
                }
%>
        </center>
    </body>
</html>