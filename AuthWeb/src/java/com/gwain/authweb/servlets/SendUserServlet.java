package com.gwain.authweb.servlets;

import com.gwain.authweb.entities.ChatUser;
import com.gwain.authweb.entities.ChatUsers;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author
 */
public class SendUserServlet extends HttpServlet {

    private String user = "";
    private Map<String, StringBuffer> usersLog;

    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        final ChatUser curuser = (ChatUser) req.getSession().getAttribute("curuser");
        final ChatUsers users =  (ChatUsers) getServletContext().getAttribute("users");
        ((List<String>)users.getUsers().get(curuser)).add(req.getParameter("mess"));
        resp.sendRedirect(resp.encodeRedirectURL("msg.jsp"));
    }
}
