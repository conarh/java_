<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% String title = "Регистрация";%>
<%@include file="WEB-INF/jspf/header.jspf" %>
    <body>
    <center>
        <h3><%= request.getAttribute("message")!=null ? request.getAttribute("message") : ""%></h3>
        <%--
            session.removeAttribute("message");
        --%>
        <form action="register" method="POST" class="form-container">
            <div class="form-title"><h2>Регистрация</h2></div>
            <div class="form-title">Username </div>
            <input class="form-field" type="text" name="user" value="" /><br>
            <div class="form-title">Password </div>
            <input class="form-field" type="password" name="pass" value="" /><br>
            <input class="submit-button" type="submit" value="Register" />
        </form>
        <hr>
        <a href="protect/msg.jsp" class="submit-button">Общение</a>
    </center>
</body>
</html>
